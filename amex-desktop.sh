#!/bin/bash

source ~/.bashrc

mkdir -p ~/iseatz/amex/desktop
cd ~/iseatz/amex/desktop

repos=(
amex
ovt
pbr
cms
cms_admin
querulous
statsd
)
for repo in ${repos[@]}
do
  if [ ! -d ./$repo ]
  then
    git clone git@bitbucket.org:iseatz/$repo.git
  fi
done

cd ~/iseatz/amex/desktop/cms_admin
if ! bundle install
then
  gem install bundler
  rbenv rehash
  bundle install
fi

# Run rake tasks to create and migrate the cms database
bundle exec rake cms:install:migrations
bundle exec rake db:create
bundle exec rake db:migrate


cd ~/iseatz/amex/desktop/amex
if [ $(uname) == "Darwin" ]
then
  # OS X needs to use gcc-4.2 to compile therubyracer gem
  export CC=/usr/local/Cellar/apple-gcc42/4.2.1-5666.3/bin/gcc-4.2
  export CXX=/usr/local/Cellar/apple-gcc42/4.2.1-5666.3/bin/g++-4.2
  export CPP=/usr/local/Cellar/apple-gcc42/4.2.1-5666.3/bin/cpp-4.2
fi
bundle install
bundle exec rake db:migrate
./fresh_db.sh

bundle exec rake suppliers:oww:refresh_car_vendor_info[fast]
bundle exec rake suppliers:oww:refresh_air_vendor_info[fast]

