#!/bin/bash

# Add .bashrc source to .bash_profile for consistency
if ! grep 'if \[ -f ~\/\.bashrc \]; then source ~\/\.bashrc; fi' ~/.bash_profile
then
  echo "# Source .bashrc to be consistent with linux" >> ~/.bash_profile
  echo "if [ -f ~/.bashrc ]; then source ~/.bashrc; fi" >> ~/.bash_profile
fi

if hash brew >/dev/null 2>&1
then
  echo "Homebrew is already installed"
else
  # Download homebrew
  echo "Installing Homebrew"
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/homebrew/install/master/install)"
  brew doctor
fi

if hash gcc-4.2 >/dev/null 2>&1
then
  echo "gcc-4.2 is already installed"
else
  # Install gcc 4.2
  brew tap homebrew/dupes
  brew install apple-gcc42
  export CC=/usr/local/Cellar/apple-gcc42/4.2.1-5666.3/bin/gcc-4.2
  export CXX=/usr/local/Cellar/apple-gcc42/4.2.1-5666.3/bin/g++-4.2
  export CPP=/usr/local/Cellar/apple-gcc42/4.2.1-5666.3/bin/cpp-4.2
fi

mkdir -p ~/Library/LaunchAgents

if hash pkg-config >/dev/null 2>&1
then
  echo "pkgconfig is already installed"
else
  # Install google protobuf
  echo "Installing pkgconfig"
  brew install pkgconfig
fi

if hash protoc >/dev/null 2>&1
then
  echo "Google Protocol Buffers is already installed"
else
  # Install google protobuf
  echo "Installing Google Protocol Buffers"
  brew install protobuf
fi

if hash postgres >/dev/null 2>&1
then
  echo "PostgreSQL is already installed"
else
  # Install postgres
  echo "Installing PostgreSQL"
  brew install postgresql
  # Set postgres to start at boot
  ln -sfv /usr/local/opt/postgresql/*.plist ~/library/LaunchAgents
  launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
fi

if hash redis-server >/dev/null 2>&1
then
  echo "Redis is already installed"
else
  # Install redis
  echo "Installing Redis"
  brew install redis
  # Set redis to start at boot
  ln -sfv /usr/local/opt/redis/*.plist ~/library/LaunchAgents
  launchctl load ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
fi


if hash memcached >/dev/null 2>&1
then
  echo "Memcached is already installed"
else
  # Install memcached
  echo "Installing Memcached"
  brew install memcached
  # Set memcached to start at boot
  ln -sfv /usr/local/opt/memcached/*.plist ~/library/LaunchAgents
  launchctl load ~/Library/LaunchAgents/homebrew.mxcl.memcached.plist
fi

# Create the postgres user; after giving the server a chance to start
echo "Creating postgres user"
createuser -s -r postgres

echo
echo "Running generic-setup.sh"
source 'generic-setup.sh'

echo
echo "OS X configuration complete"
