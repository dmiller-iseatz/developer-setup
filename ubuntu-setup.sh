#!/bin/bash

sudo apt-get update
sudo apt-get install build-essential curl libssl-dev libxml2-dev libxslt-dev libcurl3-dev uuid-dev zlib1g-dev git-core openjdk-7-jre-headless redis-server memcached postgresql-9.3 postgresql-server-dev-9.3  protobuf-compiler sqlite3 libsqlite3-dev


if ! sudo grep 'local all postgres trust' /etc/postgresql/9.3/main/pg_hba.conf
then
  # Allow local postgres user to connect without a password
  sudo cp /etc/postgresql/9.3/main/pg_hba.conf /etc/postgresql/9.3/main/pg_hba.conf.bak
  sudo cp /etc/postgresql/9.3/main/pg_hba.conf /tmp/pg_hba.conf.bak
  sudo chmod 777 /tmp/pg_hba.conf.bak
  echo "local all postgres trust" > /tmp/pg_hba.conf
  echo "host all postgres 127.0.0.1/32 trust" >> /tmp/pg_hba.conf
  cat /tmp/pg_hba.conf.bak >> /tmp/pg_hba.conf
  chmod 640 /tmp/pg_hba.conf
  sudo chown postgres:postgres /tmp/pg_hba.conf
  sudo mv /tmp/pg_hba.conf /etc/postgresql/9.3/main/pg_hba.conf
  sudo service postgresql restart
fi

echo
echo "Running generic-setup.sh"
source 'generic-setup.sh'

echo
echo "Ubuntu configuration complete"

