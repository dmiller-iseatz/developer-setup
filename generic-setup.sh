#!/bin/bash

if [ ! -d /etc/oneview.d ]
then
  # Create oneview configuration directory
  sudo mkdir -p /etc/oneview.d
  sudo chmod 777 /etc/oneview.d
fi

# Find a copy of oneview.d.tar.gz and extract it into /etc/oneview.d


if hash rbenv >/dev/null 2>&1
then
  echo "rbenv is already isntalled"
else
  # Clone rbenv repositories to your home directoyr
  echo "Installing rbenv"
  git clone git://github.com/sstephenson/rbenv.git ~/.rbenv
  git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
  # Add entries to your .bashrc to put rbenv in the $path
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
  echo 'eval "$(rbenv init -)"' >> ~/.bashrc
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
fi

# Install ruby versions that are currently in use
versions=(
1.8.7-p371
1.9.3-p392
jruby-1.7.10
)
for version in ${versions[@]}
do
  if [ ! -d ~/.rbenv/versions/$version ]
  then
    rbenv install $version
  else
    echo "$version is already installed"
  fi
done
rbenv global 1.9.3-p392

if hash nvm >/dev/null 2>&1
then
  echo "nvm is already installed"
else
  # Install nvm and install node
  echo "Installing nvm"
  git clone https://github.com/creationix/nvm.git ~/.nvm
  echo "source ~/.nvm/nvm.sh" >> ~/.bashrc
  source ~/.nvm/nvm.sh
fi

if [ ! -d ~/.nvm/v0.10.26 ]
then
  nvm install v0.10.26
  nvm use v0.10.26
  nvm alias default v0.10.26
else
  echo "Node is already installed"
fi

