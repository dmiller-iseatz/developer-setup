#!/bin/bash

source ~/.bashrc

mkdir -p ~/iseatz/amex/mobile
cd ~/iseatz/amex/mobile

repos=(
pbr
ovc
axmobile
querulous
)
for repo in ${repos[@]}
do
  if [ ! -d ./$repo ]
  then
    git clone git@bitbucket.org:iseatz/$repo.git
  fi
done

cd ~/iseatz/amex/mobile/querulous
rbenv local jruby-1.7.10
version=$(gem build querulous.gemspec |  awk '/File/ {print $2}' -)
gem install $version
git checkout .ruby-version


cd ~/iseatz/amex/mobile/axmobile
if [ ! -f /etc/oneview.d/ovc.yml ]
then
  cp config/ovc.yml /etc/oneview.d/
  vim /etc/oneview.d/ovc.yml
fi

cd ~/iseatz/amex/mobile/ovc
if ! bundle install
then
  gem install bundler
  rbenv rehash
  bundle install
fi
bundle exec rake db:create
bundle exec rake db:migrate

cd ~/iseatz/amex/mobile/axmobile
bundle install
torquebox deploy
./developer-config.sh

npm install -g gulp
npm install
gulp

