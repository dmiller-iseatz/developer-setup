source ~/.bashrc

if [ -f /usr/local/lib/libzmq.a ]
then
  echo "ZeroMQ is already installed"
else
  # Download and install zeromq
  echo "Installing ZeroMQ"
  curl http://download.zeromq.org/zeromq-2.2.0.tar.gz -o /tmp/zeromq-2.2.0.tar.gz
  cd /tmp
  tar xvfz zeromq-2.2.0.tar.gz
  cd zeromq-2.2.0/
  ./configure
  make
  sudo make install
fi


sudo mkdir -p /var/log/oneview
sudo chmod 777 /var/log/oneview

sudo ln -s ~/.nvm/v0.10.26/lib/node_modules/ /usr/local/lib/node_modules
npm install -g zmq node-uuid jasmine-node protobuf js-yaml redis

mkdir -p ~/iseatz/api
cd ~/iseatz/api

repos=(
pbr
querulous
one_view_service
ovn
hotel
hotel_orbitz
air
air_orbitz
dp
dp_orbitz
car
car_orbitz
slog
iseatz-ovpm
logd
statsd
ovbs
ov_mailer
email_pal
ovlinks
amex_mr
)
for repo in ${repos[@]}
do
  if [ ! -d ./$repo ]
  then
    git clone git@bitbucket.org:iseatz/$repo.git
  fi
done

cd ~/iseatz/api/ovn
git checkout .
git apply local-fix.diff

cd ~/iseatz/api
for x in `ls`
do
cd $x
if [ -f Gemfile ]
then
  echo "BUNDLE INSTALLING $x"
  if ! bundle install
  then
    gem install bundler
    rbenv rehash
    bundle install
  fi
fi
cd ..
done

if hash ovpm
then
  echo "ovpm gem is already installed"
else
  echo "Installing ovpm gem"
  cd ~/iseatz/api/iseatz-ovpm
  version=$(gem build ovpm.gemspec |  awk '/File/ {print $2}' -)
  gem install $version
  rbenv rehash
fi

if [ ! -d /opt/services ]
then
  sudo mkdir -p /opt
  sudo ln -s $HOME/iseatz/api /opt/services
fi

