# SSH Keys

SSH Keys are necessary to clone repositories from bitbucket.org. You can use an existing ssh key or generate a new one.

```
ssh-keygen
```
* Follow the prompts to create a ssh key.


## Add your SSH key to [Bitbucket](https://bitbucket.org/)

* Browse to the Manage Account page.
* Select the SSH Keys from the Security settings.
* Follow the instructions to add a key.

# Operating System Dependencies

There are different steps to configure different operating systems. Follow the steps for your OS.

Clone this repository then follow the instructions for your operating system.

## OS X

1. Download the Command Line Tools from Apple: https://developer.apple.com/downloads/
2. Download the Java Developer Kit (JDK) from Oracle: http://www.oracle.com/technetwork/java/javase/downloads/index.html
3. Clone this repository.
4. Run the [apple-setup.sh](developer-setup/src/HEAD/apple-setup.sh) script.
    * Installs [Homebrew](http://brew.sh/)
    * Creates the ~/Library/LaunchAgents directory.

## Ubuntu

1. Install git ```sudo apt-get install git-core```.
2. Clone this repository.
3. Run the [ubuntu-setup.sh](developer-setup/src/HEAD/ubuntu-setup.sh) script.
    * Installs required development libraries (libssl-dev, libxml2-dev, libxslt-dev), build-essential, and curl.
    * Installs [git](http://git-scm.com/).
    * Installs [SQLite](http://www.sqlite.org/).

In addition to the above, both scripts will do the following:

* Install Google [Protocol Buffers](https://developers.google.com/protocol-buffers/).
* Install [PostgreSQL](http://www.postgresql.org/) and create the ```postgres``` user.
* Install [Redis](http://redis.io/).
* Install [Memcached](http://memcached.org/).
* Execute the ```generic-setup.sh``` script, which will do the following:
    * Create the ```/etc/oneview.d``` directory.
    * Install [rbenv](https://github.com/sstephenson/rbenv).
        * Install ruby 1.8.7-p371 for EE.
        * Install ruby 1.9.3-p392 for American Express desktop.
        * Install jruby-1.7.10 for American Express mobile.
    * Install [nvm](https://github.com/creationix/nvm).
        * Install node v0.10.26

# Git configuration

* Add your user name and email to your global git config.

```
git config --global user.name "your name"
git config --global user.email "your email"
```

